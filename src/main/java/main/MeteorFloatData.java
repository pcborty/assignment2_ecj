package main;

import ec.gp.GPData;

public class MeteorFloatData extends GPData {

    public float x;

    public void copyTo(final GPData gpd) {
        ((MeteorFloatData) gpd).x = x;
    }
}
