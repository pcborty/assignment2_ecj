package main;

import org.apache.commons.lang3.ArrayUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class Utils {

    /* Inplace matrix 90 deg rotation, anticlockwise */
    public static void rotateMatrix(int N, double[][] mat) {
        for (int x = 0; x < N / 2; x++) {
            for (int y = x; y < N - x - 1; y++) {
                double temp = mat[x][y];
                mat[x][y] = mat[y][N - 1 - x];
                mat[y][N - 1 - x] = mat[N - 1 - x][N - 1 - y];
                mat[N - 1 - x][N - 1 - y] = mat[N - 1 - y][x];
                mat[N - 1 - y][x] = temp;
            }
        }
    }

    public static double[] flattenArray(double[][] mat, double label) {
        double[] flattenedArray = Arrays.stream(mat)
                .flatMapToDouble(Arrays::stream)
                .toArray();

        return ArrayUtils.add(flattenedArray, label);
    }

    public static double[] getFlattenedPixelInfo(BufferedImage image,
                                                 int i, int j) {

        /* Constructing a 5 * 5 matrix based on the pixels */
        double[][] pixelMatrix = new double[5][5];
        int idxx = 0;
        int idxy = 0;

        for (int iStart = i - 2; iStart <= i + 2; iStart++) {
            for (int jStart = j - 2; jStart <= j + 2; jStart++) {
                Color color = new Color(image.getRGB(iStart, jStart), true);

                /* For a greyscale image, the RGB values are same. We can choose anyone.
                 * Taking the value within 0 to 1 range.
                 */
                pixelMatrix[idxx][idxy] = (float) (color.getRed() / 255.0);

                idxx = (idxx + 1) % 5;
            }

            idxy = (idxy + 1) % 5;
        }

        return Arrays.stream(pixelMatrix)
                .flatMapToDouble(Arrays::stream)
                .toArray();
    }
}
