package main;

import ec.EvolutionState;
import ec.Individual;
import ec.app.tutorial4.DoubleData;
import ec.gp.GPIndividual;
import ec.gp.GPProblem;
import ec.simple.SimpleFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.ClassLoader.getSystemClassLoader;
import static java.lang.String.format;
import static java.util.Arrays.copyOfRange;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static main.Utils.flattenArray;
import static main.Utils.rotateMatrix;

public class MeteorShowerProblem extends GPProblem implements SimpleProblemForm {

    /* Choosing filter as 5x5 */
    public static final int FILTER_DIM = 5;
    public static final int FILTER_CUSHION = FILTER_DIM / 2;

    private static int count = 0;

    public double currentX1, currentX2, currentX3, currentX4, currentX5,
            currentX6, currentX7, currentX8, currentX9, currentX10,
            currentX11, currentX12, currentX13, currentX14, currentX15,
            currentX16, currentX17, currentX18, currentX19, currentX20,
            currentX21, currentX22, currentX23, currentX24, currentX25;

    public List<double[]> inputList = new ArrayList<>();

    public List<double[]> inputX = new ArrayList<>();
    public List<Double> inputY = new ArrayList<>();

    public List<double[]> trainX;
    public List<Double> trainY;

    public List<double[]> testX;
    public List<Double> testY;

    @Override
    public void setup(EvolutionState state, Parameter base) {
        super.setup(state, base);

        if (!(input instanceof DoubleData)) {
            state.output.fatal("GPData class must subclass from " + DoubleData.class,
                    base.push(P_DATA), null);
        }

        try {
            loadData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void evaluate(final EvolutionState evolutionState, final Individual individual,
                         final int subPopulation, final int threadNum) {

        if (!individual.evaluated) {

            DoubleData input = (DoubleData) (this.input);
            GPIndividual ind = (GPIndividual) individual;

            int hits = 0;
            double expectedResult, result;

            for (int i = 0; i < trainX.size(); i++) {
                double[] currentX = trainX.get(i);

                currentX1 = currentX[0];
                currentX2 = currentX[1];
                currentX3 = currentX[2];
                currentX4 = currentX[3];
                currentX5 = currentX[4];
                currentX6 = currentX[5];
                currentX7 = currentX[6];
                currentX8 = currentX[7];
                currentX9 = currentX[8];
                currentX10 = currentX[9];

                currentX11 = currentX[10];
                currentX12 = currentX[11];
                currentX13 = currentX[12];
                currentX14 = currentX[13];
                currentX15 = currentX[14];
                currentX16 = currentX[15];
                currentX17 = currentX[16];
                currentX18 = currentX[17];
                currentX19 = currentX[18];
                currentX20 = currentX[19];

                currentX21 = currentX[20];
                currentX22 = currentX[21];
                currentX23 = currentX[22];
                currentX24 = currentX[23];
                currentX25 = currentX[24];

                expectedResult = trainY.get(i);

                ind.trees[0].child.eval(evolutionState, threadNum, input, stack, ind, this);
                result = input.x > 0 ? 1 : 0;

                if (result == expectedResult) {
                    hits++;
                }
            }

            SimpleFitness f = ((SimpleFitness) ind.fitness);
            f.setFitness(evolutionState, hits / (double) trainX.size(), (hits == trainX.size()));

            ind.evaluated = true;
        }
    }

    @Override
    public void describe(EvolutionState evolutionState, Individual individual,
                         int subPopulation, int threadNum, int log) {

        super.describe(evolutionState, individual, subPopulation, threadNum, log);

        DoubleData input = (DoubleData) (this.input);
        GPIndividual ind = (GPIndividual) individual;

        int hits = 0;
        double expectedResult, result;

        int tp = 0, tn = 0, fp = 0, fn = 0;

        for (int i = 0; i < testX.size(); i++) {
            double[] currentX = testX.get(i);

            currentX1 = currentX[0];
            currentX2 = currentX[1];
            currentX3 = currentX[2];
            currentX4 = currentX[3];
            currentX5 = currentX[4];
            currentX6 = currentX[5];
            currentX7 = currentX[6];
            currentX8 = currentX[7];
            currentX9 = currentX[8];
            currentX10 = currentX[9];

            currentX11 = currentX[10];
            currentX12 = currentX[11];
            currentX13 = currentX[12];
            currentX14 = currentX[13];
            currentX15 = currentX[14];
            currentX16 = currentX[15];
            currentX17 = currentX[16];
            currentX18 = currentX[17];
            currentX19 = currentX[18];
            currentX20 = currentX[19];

            currentX21 = currentX[20];
            currentX22 = currentX[21];
            currentX23 = currentX[22];
            currentX24 = currentX[23];
            currentX25 = currentX[24];

            expectedResult = testY.get(i);

            ind.trees[0].child.eval(evolutionState, threadNum, input, stack, ind, this);
            result = input.x > 0 ? 1 : 0;

            if (result == expectedResult) {
                hits++;
            }

            if (expectedResult == 1) {
                if (result == 1) {
                    tp++;
                } else {
                    fn++;
                }
            } else {
                if (result == 1) {
                    fp++;
                } else {
                    tn++;
                }
            }
        }

        evolutionState.output.println(format("%8s|%8s|%8s", "", "RealP", "RealN"), 1);
        evolutionState.output.println(format("%8s|%8d|%8d", "GuessP", tp, fp), 1);
        evolutionState.output.println(format("%8s|%8d|%8d", "GuessN", fn, tn), 1);
        evolutionState.output.println(format("Test Accuracy: %f", hits / (double) testX.size()), 1);

        try {
            readNewImageAndDraw(evolutionState, ind, threadNum, input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadData() throws IOException {

        /* Reading the main training image */
        BufferedImage coloredWithoutLabel = ImageIO.read(requireNonNull(
                getSystemClassLoader().getResource("images/train.png")));

        /* Reading the training image with colored meteor trail */
        BufferedImage coloredWithLabel = ImageIO.read(requireNonNull(
                getSystemClassLoader().getResource("images/train_labeled.png")));

        /* Converting the training image into greyscale image */
        BufferedImage greyScaleWithoutLabel = new BufferedImage(
                coloredWithoutLabel.getWidth(), coloredWithoutLabel.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = greyScaleWithoutLabel.getGraphics();
        g.drawImage(coloredWithoutLabel, 0, 0, null);
        g.dispose();

        /* Turning the image into a dataset. We are taking the pixel itself and a 5*5 region around it. */
        for (int i = FILTER_CUSHION; i < coloredWithLabel.getWidth() - FILTER_CUSHION; i++) {
            for (int j = FILTER_CUSHION; j < coloredWithLabel.getHeight() - FILTER_CUSHION; j++) {
                inputList.addAll(getPixelsAroundTargetAndLabel(greyScaleWithoutLabel, coloredWithLabel, i, j));
            }
        }

        /* There are more instances of regular pixel than meteor trail,
         * and we have to balance the dataset. First, we are separating
         * the non-meteor trail pixels and choosing a portion of it, matching
         * the number of instances with meteor trail.
         */
        List<double[]> label0Instances = inputList.stream()
                .filter(arr -> arr[FILTER_DIM * FILTER_DIM] == 0)
                .collect(toList());
        List<double[]> label1Instances = inputList.stream()
                .filter(arr -> arr[FILTER_DIM * FILTER_DIM] == 1)
                .collect(toList());

        Collections.shuffle(label0Instances);
        label0Instances = label0Instances.subList(0, label1Instances.size());

        List<double[]> balancedList = new ArrayList<>(label0Instances);
        balancedList.addAll(label1Instances);
        Collections.shuffle(balancedList);

        inputX = balancedList.stream()
                .map(arr -> copyOfRange(arr, 0, FILTER_DIM * FILTER_DIM))
                .collect(toList());
        inputY = balancedList.stream().map(arr -> arr[FILTER_DIM * FILTER_DIM]).collect(toList());

        /* Taking 40% of the data as training data and the rest as test data */
        trainX = inputX.subList(0, (int) (inputX.size() * 0.9));
        trainY = inputY.subList(0, (int) (inputX.size() * 0.9));

        testX = inputX.subList((int) (inputX.size() * 0.9), inputX.size());
        testY = inputY.subList((int) (inputX.size() * 0.9), inputY.size());
    }

    private boolean isPixelInMeteorTrail(BufferedImage image, int i, int j) {
        Color color = new Color(image.getRGB(i, j), true);

        /* Thresholds selected by trial and error */
        return color.getRed() > 250
                && color.getGreen() < 20
                && color.getBlue() < 20;
    }

    private List<double[]> getPixelsAroundTargetAndLabel(BufferedImage image, BufferedImage coloredWithLabel,
                                                         int i, int j) {

        /* Constructing a 5 * 5 matrix based on the pixels */
        double[][] pixelMatrix = new double[FILTER_DIM][FILTER_DIM];
        int idxx = 0;
        int idxy = 0;

        for (int iStart = i - FILTER_CUSHION; iStart <= i + FILTER_CUSHION; iStart++) {
            for (int jStart = j - FILTER_CUSHION; jStart <= j + FILTER_CUSHION; jStart++) {
                Color color = new Color(image.getRGB(iStart, jStart), true);

                /* For a greyscale image, the RGB values are same. We can choose any.
                 * Taking the value within 0 to 1 range.
                 */
                pixelMatrix[idxx][idxy] = (float) (color.getRed() / 255.0);

                idxx = (idxx + 1) % FILTER_DIM;
            }

            idxy = (idxy + 1) % FILTER_DIM;
        }

        double label = isPixelInMeteorTrail(coloredWithLabel, i, j) ? 1 : 0;

        /* Rotating the matrix 3 times to get 4 data points out of the same pixel.
         * This is called image augmentation. There are more sophisticated approaches
         * to do this, but this is the most basic one.
         */
        List<double[]> augmentedData = new ArrayList<>();

        augmentedData.add(flattenArray(pixelMatrix, label));

        for (int m = 1; m <= 3; m++) {
            rotateMatrix(FILTER_DIM, pixelMatrix);
            augmentedData.add(flattenArray(pixelMatrix, label));
        }

        return augmentedData;
    }

    private void readNewImageAndDraw(EvolutionState evolutionState, GPIndividual ind,
                                     int threadNum, DoubleData input) throws IOException {

        BufferedImage coloredImage = ImageIO.read(requireNonNull(
                getSystemClassLoader().getResource("images/test.png")));

        BufferedImage greyScaleImage = new BufferedImage(
                coloredImage.getWidth(), coloredImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = greyScaleImage.getGraphics();
        g.drawImage(coloredImage, 0, 0, null);
        g.dispose();

        for (int i = FILTER_CUSHION; i < coloredImage.getWidth() - FILTER_CUSHION; i++) {
            for (int j = FILTER_CUSHION; j < coloredImage.getHeight() - FILTER_CUSHION; j++) {
                double[] currentX = Utils.getFlattenedPixelInfo(greyScaleImage, i, j);

                currentX1 = currentX[0];
                currentX2 = currentX[1];
                currentX3 = currentX[2];
                currentX4 = currentX[3];
                currentX5 = currentX[4];
                currentX6 = currentX[5];
                currentX7 = currentX[6];
                currentX8 = currentX[7];
                currentX9 = currentX[8];
                currentX10 = currentX[9];

                currentX11 = currentX[10];
                currentX12 = currentX[11];
                currentX13 = currentX[12];
                currentX14 = currentX[13];
                currentX15 = currentX[14];
                currentX16 = currentX[15];
                currentX17 = currentX[16];
                currentX18 = currentX[17];
                currentX19 = currentX[18];
                currentX20 = currentX[19];

                currentX21 = currentX[20];
                currentX22 = currentX[21];
                currentX23 = currentX[22];
                currentX24 = currentX[23];
                currentX25 = currentX[24];

                ind.trees[0].child.eval(evolutionState, threadNum, input, stack, ind, this);
                int result = input.x > 0 ? 1 : 0;

                if (result == 1) {
                    coloredImage.setRGB(i, j, Color.RED.getRGB());
                }
            }
        }

        File outputFile = new File("image" + count + ".png");
        ImageIO.write(coloredImage, "png", outputFile);
        count++;
    }
}
